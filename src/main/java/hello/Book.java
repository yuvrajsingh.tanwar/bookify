package hello;

import java.util.Objects;

public class Book {
    private String bookName="Davinci";
    public Book(String name ) {
        this.bookName = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(bookName, book.bookName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookName);
    }

    public String getBookName() {
        return bookName;
    }
}
