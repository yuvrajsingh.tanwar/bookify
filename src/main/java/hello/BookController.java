package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {
    @RequestMapping(value = "/application", method = RequestMethod.GET)
    public Book book(@RequestParam(value = "name", defaultValue = "Vijay & Yuvraj") String name) {
        return new Book(name);
    }

    @RequestMapping(value = "/app", method = RequestMethod.GET)
    public String books(){
        return "1223";
    }

}
